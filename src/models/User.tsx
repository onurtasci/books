interface User {
    id: number
    name: string
    birthdate: string
    gender: string
}

export default User
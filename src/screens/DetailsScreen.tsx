import React, { useEffect, useState } from 'react'
import { StyleSheet, View, Text, Image, Dimensions, ActivityIndicator } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import ButtonIcon from '../components/UI/ButtonIcon'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Book from '../models/Book';

const deviceWidth = Dimensions.get('window').width

interface Props {
    route: {
        params: {
            id: string
        }
    }
    navigation: {
        navigate: (param: string) => void
        goBack: () => void
        setOptions: (params: object) => void
        addListener: (param: string, func: () => void) => void
    };
}

const DetailsScreen: React.FC<Props> = (props) => {
    const [loading, setLoading] = useState<boolean>(false)
    const [item, setItem] = useState<Book | null>(null)

    const id = props.route.params.id

    useEffect(() => {
        props.navigation?.setOptions({
            headerTitle: '',
            headerLeft: () => <ButtonIcon style={{ marginLeft: 16, marginTop: 28 }} name='back' size={32} onPress={() => props.navigation.goBack()} loading={false} />,
            headerStyle: {
                elevation: 0,
                shadowRadius: 0,
                shadowOffset: {
                    height: 0,
                },
                height: 92,
            },
        })
    }, [])

    useEffect(() => {
        // const unsubscribe = props.navigation.addListener('focus', getBook)
        // return () => {
        //     unsubscribe()
        // };
        getBook()
    }, []);

    const getBook = async () => {
        setLoading(true)
        try {
            const userInfo = await AsyncStorage.getItem('userInfo')
            if (!userInfo) {
                console.log('USER DOES NOT EXIST')
                setLoading(false)
                return
            } else {
                const transformedData = JSON.parse(userInfo)
                const res = await fetch(`https://books.ioasys.com.br/api/v1/books/${id}`, {
                    method: 'GET',
                    headers: {
                        'accept': 'application/json',
                        'authorization': 'Bearer ' + transformedData.authToken,
                    }
                })
                const result = await res.json()
                setItem(result)
                if (result.errors) {
                    setLoading(false)
                    return
                }
                setLoading(false)
            }
        } catch (err) {
            setLoading(false)
        }
    }

    if(loading) {
        return <ActivityIndicator style={{marginTop: 16}} size='large' color='#B22E6F' />
    }

    return (
        <ScrollView style={styles.screen} contentContainerStyle={styles.scroll} >
            <Image style={styles.image} source={{ uri: item?.imageUrl }} />
            <Text style={styles.title} numberOfLines={2}>{item?.title}</Text>
            <Text style={styles.author}>{item?.authors?.join(', ')}</Text>

            <Text style={{ ...styles.label, marginBottom: 16 }}>INFORMAÇÕES</Text>

            <View style={styles.infoRow} >
                <Text style={styles.label}>Páginas</Text>
                <Text style={styles.value}>{item?.pageCount}</Text>
            </View>
            <View style={styles.infoRow} >
                <Text style={styles.label}>Editora</Text>
                <Text style={styles.value}>{item?.publisher}</Text>
            </View>
            <View style={styles.infoRow} >
                <Text style={styles.label}>Publicação</Text>
                <Text style={styles.value}>{item?.published}</Text>
            </View>
            <View style={styles.infoRow} >
                <Text style={styles.label}>Idioma</Text>
                <Text style={styles.value}>{item?.language}</Text>
            </View>
            <View style={styles.infoRow} >
                <Text style={styles.label}>Título Original</Text>
                <Text style={styles.value}>{item?.title}</Text>
            </View>
            <View style={styles.infoRow} >
                <Text style={styles.label}>ISBN-10</Text>
                <Text style={styles.value}>{item?.isbn10}</Text>
            </View>
            <View style={styles.infoRow} >
                <Text style={styles.label}>ISBN-13</Text>
                <Text style={styles.value}>{item?.isbn13}</Text>
            </View>
            <View style={styles.infoRow} >
                <Text style={styles.label}>Categoria</Text>
                <Text style={styles.value}>{item?.category}</Text>
            </View>

            <Text style={{ ...styles.label, marginTop: 16, marginBottom: 16 }}>RESENHA DA EDITORA</Text>
            <Text style={styles.description}>
                <View style={{ paddingRight: 5 }}>
                    <Image style={{ height: 14.14, width: 17.58 }} source={require('../assets/icons/desc.png')} />
                </View>
                {item?.description}
            </Text>

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#FFFFFF'
    },
    scroll: {
        paddingHorizontal: 40,
        paddingVertical: 16,
    },
    image: {
        width: deviceWidth - 80,
        height: (deviceWidth - 80) * 1.458,
        marginRight: 16
    },
    title: {
        fontWeight: '500',
        fontSize: 28,
        color: '#333333',
        marginTop: 24
    },
    author: {
        fontWeight: '400',
        fontSize: 12,
        color: '#AB2680',
        marginBottom: 32
    },
    infoRow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 20
    },
    label: {
        fontWeight: '500',
        fontSize: 12,
        color: '#333333',
    },
    value: {
        fontWeight: '400',
        fontSize: 12,
        color: '#999999',
    },
    description: {
        fontWeight: '500',
        fontSize: 12,
        color: '#999999',
    },
})

export default DetailsScreen
import React, { useEffect, useState, useCallback } from 'react'
import { StyleSheet, View, Text, ScrollView, Image, ActivityIndicator, Alert } from 'react-native'
import BookItem from '../components/items/BookItem'
import FilterModal from '../components/items/FilterModal'
import ButtonIcon from '../components/UI/ButtonIcon'
import SearchBar from '../components/UI/SearchBar'
import Book from '../models/Book'
import AsyncStorage from '@react-native-async-storage/async-storage';

interface Props {
    navigation: {
        navigate: (arg1: string, arg2: object) => void
        setOptions: (params: object) => void
        addListener: (param: string, func: () => void) => void | any
    };
}

const HomeScreen: React.FC<Props> = (props) => {
    const [loading, setLoading] = useState<boolean>(false)
    const [searchedItems, setSearchedItems] = useState<Book[]>([])
    const [books, setBooks] = useState<Book[]>([])
    const [currentPage, setCurrentPage] = useState<number>(1)

    const itemsPerPage = 10

    useEffect(() => {
        props.navigation?.setOptions({
            header: () => (
                <View style={styles.header}>
                    <View style={styles.top} >
                        <View style={styles.titleContainer} >
                            <Image source={require('../assets/images/logo_home.png')} style={styles.image} />
                            <Text style={styles.titleRight} >Books</Text>
                        </View>
                        <ButtonIcon style={{}} name='logout' size={32} onPress={askForLogout} loading={false} />
                    </View>
                    <View style={styles.bottom}>
                        <SearchBar
                            data={books}
                            setFixedData={(val) => { setSearchedItems(val) }}
                        />
                        <FilterModal onPressFilter={(cats, years) => { console.log('Filtered Items: ', cats, years) }} />
                    </View>
                </View>
            ),
            headerLeft: null,
            headerStyle: {
                elevation: 0,
                shadowRadius: 0,
                shadowOffset: {
                    height: 0,
                },
                backgroundColor: '#E5E5E5'
            },
        })
    }, [])

    useEffect(() => {
        const unsubscribe = props.navigation.addListener('focus', getBooks.bind(this, currentPage))
        return () => {
            unsubscribe()
        };
    }, []);

    //desafio@ioasys.com.br

    const getBooks = async (page: number) => {
        setLoading(true)
        try {
            const userInfo = await AsyncStorage.getItem('userInfo')
            if (!userInfo) {
                console.log('USER DOES NOT EXIST')
                setLoading(false)
                return
            } else {
                const transformedData = JSON.parse(userInfo)
                const res = await fetch(`https://books.ioasys.com.br/api/v1/books?page=${page}&amount=${itemsPerPage}`, {
                    method: 'GET',
                    headers: {
                        'accept': 'application/json',
                        'authorization': 'Bearer ' + transformedData.authToken,
                    }
                })
                const result = await res.json()
                const previousList = [...books]
                const newList = previousList.concat(result.data)
                setBooks(newList)
                setSearchedItems(newList)
                console.log('RESULT: ', result.data.length)
                if (result.errors) {
                    setLoading(false)
                    return
                }
                setLoading(false)
            }
        } catch (err) {
            setLoading(false)
        }
    }

    const askForLogout = () => {
        Alert.alert(
            'Sair do ioasys Books?',
            '',
            [
                { text: 'Cancelar', style: 'default', onPress: () => {} },
                { text: 'Sair', style: 'destructive', onPress: logoutHandler }
            ]
        );
    }

    const logoutHandler = async () => {
        try {
            await AsyncStorage.removeItem('userInfo')
            props.navigation.navigate('Login', {})
        } catch (error) {
            console.log(error)
        }
    }

    const isCloseToBottom: (lm: any) => boolean = ({ layoutMeasurement, contentOffset, contentSize }) => layoutMeasurement.height + contentOffset.y >= contentSize.height - 20;

    return (
        <ScrollView
            style={styles.screen}
            contentContainerStyle={styles.scroll}
            onScroll={({ nativeEvent }) => {
                if (isCloseToBottom(nativeEvent) && currentPage * itemsPerPage <= books?.length) {
                    setCurrentPage(currentPage + 1)
                    getBooks(currentPage + 1)
                }
                if (isCloseToBottom(nativeEvent) && currentPage * itemsPerPage > books?.length && !loading) {
                    // setBottomText('Fim da lista')
                }
            }}
            scrollEventThrottle={400}
        >
            {searchedItems.map((item: Book, index: number) => (
                <BookItem item={item} key={index.toString()} onPress={() => props.navigation.navigate('Details', {id: item.id})} />
            ))}
            {loading && <ActivityIndicator style={{ marginTop: 17 }} size='small' color='#B22E6F' />}
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#E5E5E5'
    },
    scroll: {
        paddingHorizontal: 16,
    },
    header: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 42,
        paddingHorizontal: 16,
        backgroundColor: '#E5E5E5'
    },
    top: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 34

    },
    image: {
        width: 104.4,
        height: 36,
        marginRight: 16
    },
    titleRight: {
        fontWeight: '300',
        fontSize: 28,
        color: '#333333',
    },
    bottom: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 16
    }
})

export default HomeScreen
import React, { useEffect, useState } from 'react'
import { ImageBackground, StyleSheet, View, Text, Image, ScrollView } from 'react-native'
import InputBox from '../components/UI/InputBox'
import AsyncStorage from '@react-native-async-storage/async-storage';

interface Props {
    navigation: {
        navigate: (param: string) => void
        setOptions: (params: object) => void
    };
}

const LoginScreen: React.FC<Props> = (props) => {
    const [loading, setLoading] = useState<boolean>(false)
    const [errorMessage, setErrorMessage] = useState<string>(' ')

    const [email, setEmail] = useState<string>('')
    const [password, setPassword] = useState<string>('')

    const [invalidInputs, setInvalidInputs] = useState<any[]>(['email', 'senha'])

    useEffect(() => {
        props.navigation?.setOptions({
            headerTitle: '',
            headerLeft: null,
            headerStyle: {
                elevation: 0,
                shadowRadius: 0,
                shadowOffset: {
                    height: 0,
                },
                height: 0
            },
        })
    }, [])

    const loginHandler = async () => {
        if (invalidInputs?.length > 0) {
            setErrorMessage('Email ou senha é inválido')
            console.log(invalidInputs)
            return
        }
        setLoading(true)
        setErrorMessage(' ')
        try {
            const res: any = await fetch(`https://books.ioasys.com.br/api/v1/auth/sign-in`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    'email': email,
                    'password': password
                })
            })
            const result = await res.json()
            console.log('RESULT: ', result)
            if (result.errors) {
                setErrorMessage(result.errors.message)
                setLoading(false)
                return
            }
            const authToken = res.headers.map.authorization
            const refreshToken = res.headers.map['refresh-token']
            await AsyncStorage.setItem('userInfo', JSON.stringify({
                user: result,
                authToken,
                refreshToken
            }))
            setLoading(false)
            props.navigation.navigate('Home')
        } catch (err) {
            setLoading(false)
        }
    }

    const onChangeEmail = (val: string) => {
        const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!emailRegex.test(val.toLowerCase())) {
            const errorExist = invalidInputs.includes('email')
            if (!errorExist) {
                const newList = invalidInputs
                newList.push('email')
                setInvalidInputs(newList)
            }
        } else {
            setInvalidInputs(invalidInputs.filter(inv => inv !== 'email'))
        }
        setEmail(val.trim())
    }

    const onChangepassword = (val: string) => {
        setPassword(val)
        if (val.length < 6) {
            const errorExist = invalidInputs.includes('senha')
            if (!errorExist) {
                setInvalidInputs([...invalidInputs, 'senha'])
            }
        } else {
            setInvalidInputs(invalidInputs.filter(inv => inv !== 'senha'))
        }
    }

    return (
        <ImageBackground style={styles.container} source={require('../assets/images/login_background.png')} resizeMode="cover" >
            <ScrollView contentContainerStyle={styles.scroll} >
                <View style={styles.titleContainer} >
                    <Image source={require('../assets/images/logo.png')} style={styles.image} />
                    <Text style={styles.titleRight} >Books</Text>
                </View>
                <InputBox
                    type='email'
                    value={email}
                    onChangeText={onChangeEmail}
                    label='E-Mail'
                    error={invalidInputs.find(inv => inv === 'email')}
                    testID='email.input'
                    onPressButton={() => { }}
                    loading={false}
                />
                <InputBox
                    type='password'
                    value={password}
                    onChangeText={onChangepassword}
                    label='Senha'
                    error={invalidInputs.find(inv => inv === 'senha')}
                    testID='password.input'
                    onPressButton={loginHandler}
                    loading={loading}
                />
                <Text style={styles.error}>{errorMessage}</Text>
            </ScrollView>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 16
    },
    scroll: {
        flex: 1,
        paddingHorizontal: 16,
        justifyContent: 'center'
    },
    titleContainer: {
        flexDirection: 'row',
        marginBottom: 86

    },
    image: {
        width: 104.4,
        height: 36,
        marginRight: 16
    },
    titleRight: {
        fontWeight: '300',
        fontSize: 28,
        color: '#FFFFFF',
    },
    input: {
        marginBottom: 20,
    },
    error: {
        fontSize: 16,
        color: 'yellow',
        textAlign: 'center'
    }
})

export default LoginScreen
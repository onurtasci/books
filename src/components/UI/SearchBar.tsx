import React, { useState, useEffect } from 'react'
import { StyleSheet, View, TextInput, Dimensions } from 'react-native'
import Book from '../../models/Book'
import CustomIcon from './CustomIcon'

const deviceWidth = Dimensions.get('window').width

interface Props {
    data: Book[]
    setFixedData: (data: Book[]) => void
}

const SearchBar: React.FC<Props> = props => {
    const [input, setInput] = useState<string>()
    const [inMemoryData, setInMemoryData] = useState<Book[]>([])

    useEffect(() => {
        setInMemoryData(props.data)
    }, [props.data])

    const searchHandler = (value: string) => {
        setInput(value)
        const filteredData = inMemoryData.filter(
            (item: Book) => {
                let itemLowerCase1 = item.title.toLowerCase()
                let itemLowerCase2 = item.publisher.toLowerCase()

                let searchTermLowerCase = value.toLowerCase()

                return (
                    itemLowerCase1.indexOf(searchTermLowerCase) > -1
                    || itemLowerCase2.indexOf(searchTermLowerCase) > -1
                )
            }
        )
        props.setFixedData(filteredData)
    }

    return (
        <View style={styles.searchBar} >
            <TextInput style={styles.input}
                value={input}
                placeholderTextColor='#999999'
                placeholder='Procure um livro'
                onChangeText={(value) => searchHandler(value)}
            />
            <CustomIcon name='search' size={17.08} />
        </View>
    )
}

const styles = StyleSheet.create({
    searchBar: {
        width: deviceWidth - 68,
        height: 48,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#999999',
        borderRadius: 2,
        paddingHorizontal: 10,
    },
    input: {
        flex: 1,
        fontWeight: '500',
        fontSize: 12,
        marginRight: 10,
        color: '#808080'
    }
})

export default SearchBar
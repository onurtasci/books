import React, { useState } from 'react'
import { StyleSheet, View, TextInput, Text, ActivityIndicator, Platform } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

interface Props {
    type: string;
    error: boolean;
    label: string;
    value: string;
    onChangeText: (val: string) => void;
    testID: string;
    onPressButton: () => void;
    loading: boolean;
};

const InputBox: React.FC<Props> = (props) => {
    const [editing, setEditing] = useState(false)
    const [watchError, setWatchError] = useState(false)

    const errorMessage = props.type === 'email' ? 'E-mail não é válido!' : 'Senha não é válida!'

    return (
        <View style={styles.container}>
            <View style={{ ...styles.inputContainer }} >
                <Text style={styles.error} >{editing ? '' : (watchError && props.error ? errorMessage : '')}</Text>
                {props.label &&
                    <Text style={styles.label} >{props.label}</Text>
                }
                <TextInput
                    style={styles.input}
                    onChangeText={(val: string) => { props.onChangeText(val) }}
                    onTouchStart={() => setEditing(true)}
                    onBlur={() => { setEditing(false); setWatchError(true) }}
                    value={props.value}
                    autoCorrect={false}
                    autoCapitalize='none'
                    multiline={false}
                    secureTextEntry={props.type === 'password' ? true : false}
                    testID={props.testID}
                />
            </View>
            {props.type === 'password' &&
                <TouchableOpacity style={{ ...styles.button, backgroundColor: props.loading ? 'transparent' : '#FFFFFF' }} onPress={props.onPressButton}>
                    {props.loading
                        ? <ActivityIndicator size='small' color='#FFFFFF' />
                        : <Text style={styles.buttonText}>Entrar</Text>}
                </TouchableOpacity>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 60,
        backgroundColor: 'rgba(0,0,0,0.32)',
        borderRadius: 4,
        paddingHorizontal: 8,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 16
    },
    label: {
        fontWeight: '400',
        fontSize: 12,
        color: '#FFFFFF'
    },
    inputContainer: {
        height: '100%',
        width: '70%',
        justifyContent: 'space-around',
    },
    input: {
        width: '100%',
        height: Platform.OS === 'android' ? 40 : 24,
        fontWeight: '400',
        fontSize: 16,
        color: '#FFFFFF'
    },
    button: {
        width: 85,
        height: 36,
        borderRadius: 44,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        fontWeight: '500',
        fontSize: 16,
        color: '#B22E6F'
    },
    error: {
        fontWeight: '400',
        fontSize: 12,
        color: 'yellow',
        position: 'absolute',
        left: 50
    },
})

export default InputBox
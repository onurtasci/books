import React, { useState, useRef } from 'react'
import { StyleSheet, View, Text, Modal, TouchableOpacity } from 'react-native'
import ButtonIcon from '../UI/ButtonIcon'

interface Props {
    onPressFilter: (cats: string[], years: string[]) => void;
};

const FilterModal: React.FC<Props> = props => {
    const [modalVisible, setModalVisible] = useState(false)
    const [selectedCategories, setSelectedCategories] = useState<string[]>([])
    const [selectedYears, setSelectedYears] = useState<string[]>([])

    const categorySelectionHandler = (name: string) => {
        let newList: string[] = [...selectedCategories]
        const exists = newList.find(c => c === name)
        if (exists) {
            newList = newList.filter(c => c !== name)
        } else {
            newList.push(name)
        }
        setSelectedCategories(newList)
    }

    const yearSelectionHandler = (name: string) => {
        let newList: string[] = [...selectedYears]
        const exists = newList.find(c => c === name)
        if (exists) {
            newList = newList.filter(c => c !== name)
        } else {
            newList.push(name)
        }
        setSelectedYears(newList)
    }

    return (
        <View>
            <View>
                <ButtonIcon style={{ marginLeft: 16 }} name='filter' size={20} onPress={() => { setModalVisible(true) }} loading={false} />
            </View>
            <Modal visible={modalVisible} animationType='fade' transparent={true} >
                <View style={styles.itemsContainer} >
                    <View style={styles.container} >
                        <ButtonIcon style={styles.closeButton} name='close' size={32} onPress={() => { setModalVisible(false) }} loading={false} />
                        <Text style={styles.title}>Selecione a categoria</Text>
                        <View style={styles.grid} >
                            {categories.map(item => (
                                <TouchableOpacity
                                    key={item.id}
                                    style={selectedCategories.find(c => c === item.name) ? styles.filterItemSelected : styles.filterItem}
                                    onPress={categorySelectionHandler.bind(this, item.name)}
                                >
                                    <Text
                                        style={selectedCategories.find(c => c === item.name) ? styles.filterTextSelected : styles.filterText}
                                    >
                                        {item.name}</Text>
                                </TouchableOpacity>
                            ))}
                        </View>
                        <Text style={styles.title}>Selecione a ano</Text>
                        <View style={styles.grid} >
                            {years.map(item => (
                                <TouchableOpacity
                                    key={item.id}
                                    style={selectedYears.find(c => c === item.name) ? styles.filterItemSelected : styles.filterItem}
                                    onPress={yearSelectionHandler.bind(this, item.name)}
                                >
                                    <Text
                                        style={selectedYears.find(c => c === item.name) ? styles.filterTextSelected : styles.filterText}
                                    >
                                        {item.name}
                                    </Text>
                                </TouchableOpacity>
                            ))}
                        </View>

                        <TouchableOpacity style={styles.button} onPress={props.onPressFilter.bind(this, selectedCategories, selectedYears)} >
                            <Text style={styles.buttonText} >Filtrar</Text>
                        </TouchableOpacity>

                    </View>
                </View>
            </Modal>
        </View>
    )
}

const categories = [
    { id: '1', name: 'Design' },
    { id: '2', name: 'UX Design' },
    { id: '3', name: 'UI Design' },
    { id: '4', name: 'Arquitetura da informação' },
    { id: '5', name: 'CSS' },
    { id: '6', name: 'Usabilidade' },
    { id: '7', name: 'Design Thinking' },
]

const years = [
    { id: '1', name: '2015' },
    { id: '2', name: '2016' },
    { id: '3', name: '2017' },
    { id: '4', name: '2018' },
    { id: '5', name: '2019' },
    { id: '6', name: '2020' },
    { id: '7', name: '2021' },
]

const styles = StyleSheet.create({
    itemsContainer: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.4)',
        paddingHorizontal: 16,
        alignItems: 'center',
        justifyContent: 'center'
    },
    container: {
        width: '100%',
        paddingTop: 50,
        paddingBottom: 32,
        paddingHorizontal: 10,
        borderRadius: 4,

        backgroundColor: 'white',
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 8,
        elevation: 5
    },
    closeButton: {
        position: 'absolute',
        top: 16,
        right: 16
    },
    title: {
        fontWeight: '500',
        fontSize: 12,
        color: '#333333',
        marginBottom: 8
    },
    grid: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginBottom: 37,
        alignSelf: 'center',
    },
    filterItem: {
        height: 32,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#333333',
        borderRadius: 16,
        marginRight: 12,
        paddingHorizontal: 16,
        marginBottom: 8
    },
    filterItemSelected: {
        height: 32,
        backgroundColor: '#333333',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: '#333333',
        borderRadius: 16,
        marginRight: 12,
        paddingHorizontal: 16,
        marginBottom: 8
    },
    filterText: {
        fontSize: 12,
        fontWeight: '400',
        color: '#333333',
    },
    filterTextSelected: {
        fontSize: 12,
        fontWeight: '400',
        color: '#FFFFFF',
    },
    button: {
        width: 91,
        height: 36,
        borderRadius: 18,
        borderWidth: 1,
        borderColor: '#B22EF6',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    buttonText: {
        fontWeight: '500',
        fontSize: 16,
        color: '#B22EF6',
    },
})

export default FilterModal
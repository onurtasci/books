import React from 'react'
import {StyleSheet, TouchableOpacity, View, Image, Text } from 'react-native'

interface Props {
    item: any;
    onPress: () => void
};

const BookItem: React.FC<Props> = props => {
    return (
        <TouchableOpacity style={styles.container} onPress={props.onPress}>
            <Image style={styles.image} source={{uri: props.item?.imageUrl}}/>
            <View style={styles.infoContainer}>
                <View style={styles.titleContainer} >
                    <Text style={styles.title} >{props.item?.title}</Text>
                    <Text style={styles.subTitle} >{props.item?.authors?.join('\n')}</Text>
                </View>
                <View style={styles.detailContainer} >
                    <Text style={styles.info} >{props.item.pageCount} paginas</Text>
                    <Text style={styles.info} >{props.item.category}</Text>
                    <Text style={styles.info} >Publicado em {props.item.published}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container :{
        width: '100%',
        height: 160,
        alignItems: 'center',
        // justifyContent: 'space-between',
        flexDirection: 'row',
        paddingVertical: 19,
        paddingHorizontal: 16,
        backgroundColor: '#FFFFFF',
        borderRadius: 4,
        marginTop: 16,

        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 8,
        elevation: 5,
    },
    image :{
        width: '30%',
        height: 122,
        resizeMode: 'cover',
    },
    infoContainer :{
        width: '55%',
        justifyContent: 'space-between',
        height: '100%',
        marginLeft: 16
    },
    titleContainer :{},
    detailContainer :{},
    title :{
        fontWeight: '500',
        fontSize: 14,
        color: '#333333'
    },
    subTitle :{
        fontWeight: '400',
        fontSize: 12,
        color: '#AB2680'
    },
    info :{
        fontWeight: '400',
        fontSize: 12,
        color: '#999999'
    },
})

export default BookItem
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'
import Colors from '../constants/colors';

import LoginScreen from '../screens/LoginScreen';
import HomeScreen from '../screens/HomeScreen';
import DetailsScreen from '../screens/DetailsScreen';

const defaultNavOptions = {
    headerStyle: {
        backgroundColor: Colors.sgray
    },
    headerTintColor: Colors.secondary,
    headerBackTitleVisible: false
};

const MainStack = createStackNavigator()

export const MainNavigator = () => {

    return (
        <MainStack.Navigator screenOptions={defaultNavOptions} initialRouteName='Login' >
            <MainStack.Screen
                name="Login"
                component={LoginScreen}
            />
            <MainStack.Screen
                name="Home"
                component={HomeScreen}
            />
            <MainStack.Screen
                name="Details"
                component={DetailsScreen}
            />
        </MainStack.Navigator>
    )
}